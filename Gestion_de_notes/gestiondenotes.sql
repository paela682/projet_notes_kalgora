-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mar. 22 fév. 2022 à 12:06
-- Version du serveur :  8.0.21
-- Version de PHP : 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `gestiondenotes`
--

-- --------------------------------------------------------

--
-- Structure de la table `enseignants`
--

DROP TABLE IF EXISTS `enseignants`;
CREATE TABLE IF NOT EXISTS `enseignants` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Nom` varchar(30) DEFAULT NULL,
  `Prenom` varchar(30) DEFAULT NULL,
  `Username` varchar(30) DEFAULT NULL,
  `Password` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `enseignants`
--

INSERT INTO `enseignants` (`Id`, `Nom`, `Prenom`, `Username`, `Password`) VALUES
(1, 'AFATE', 'ayelim', '@ayelim', '1234');

-- --------------------------------------------------------

--
-- Structure de la table `etudiants`
--

DROP TABLE IF EXISTS `etudiants`;
CREATE TABLE IF NOT EXISTS `etudiants` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Nom` varchar(30) DEFAULT NULL,
  `Prenom` varchar(30) DEFAULT NULL,
  `Age` int DEFAULT NULL,
  `Sexe` varchar(30) DEFAULT NULL,
  `Username` varchar(30) DEFAULT NULL,
  `Password` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `etudiants`
--

INSERT INTO `etudiants` (`Id`, `Nom`, `Prenom`, `Age`, `Sexe`, `Username`, `Password`) VALUES
(1, 'YOUROUKOU', 'Charles', NULL, NULL, 'charle@charly', '2000'),
(2, 'KADANGA', 'grace', NULL, NULL, 'grace@marie', '2001');

-- --------------------------------------------------------

--
-- Structure de la table `inbox`
--

DROP TABLE IF EXISTS `inbox`;
CREATE TABLE IF NOT EXISTS `inbox` (
  `Id_enseignant` int DEFAULT NULL,
  `Contenu` text,
  KEY `Id_enseignant` (`Id_enseignant`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `notes`
--

DROP TABLE IF EXISTS `notes`;
CREATE TABLE IF NOT EXISTS `notes` (
  `Id_Etudiant` int DEFAULT NULL,
  `Ue_id` varchar(5) DEFAULT NULL,
  `Note` float DEFAULT NULL,
  KEY `Id_Etudiant` (`Id_Etudiant`),
  KEY `Ue_id` (`Ue_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `reclamation`
--

DROP TABLE IF EXISTS `reclamation`;
CREATE TABLE IF NOT EXISTS `reclamation` (
  `Id_etudiant` int DEFAULT NULL,
  `CodeUE` varchar(5) DEFAULT NULL,
  `NoteConteste` float DEFAULT NULL,
  KEY `Id_etudiant` (`Id_etudiant`),
  KEY `CodeUE` (`CodeUE`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ues`
--

DROP TABLE IF EXISTS `ues`;
CREATE TABLE IF NOT EXISTS `ues` (
  `CodeUE` varchar(5) NOT NULL,
  `Libelle` varchar(30) DEFAULT NULL,
  `Credits` int DEFAULT NULL,
  `idenseignant` int NOT NULL,
  PRIMARY KEY (`CodeUE`),
  KEY `idenseignant` (`idenseignant`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ues`
--

INSERT INTO `ues` (`CodeUE`, `Libelle`, `Credits`, `idenseignant`) VALUES
('4578', 'CONCEPTION de uml', 4, 1),
('1478', 'GESTION DE PROJET', 4, 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
