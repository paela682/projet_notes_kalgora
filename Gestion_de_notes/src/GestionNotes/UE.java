package GestionNotes;

public class UE {
	private String codeUE;
	private String libelle;
	private int credits;
	private Enseignants enseignant;
	public String getCodeUE() {
		return codeUE;
	}
	public void setCodeUE(String codeUE) {
		this.codeUE = codeUE;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public int getCredits() {
		return credits;
	}
	public void setCredits(int credits) {
		this.credits = credits;
	}
	public Enseignants getEnseignant() {
		return enseignant;
	}
	public void setEnseignant(Enseignants enseignant) {
		this.enseignant = enseignant;
	}
	
	

}
